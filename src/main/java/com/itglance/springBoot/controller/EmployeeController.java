package com.itglance.springBoot.controller;

import com.itglance.springBoot.entity.Employee;
import com.itglance.springBoot.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/")
public class EmployeeController {

    @Autowired
    EmployeeService employeeService;

    @RequestMapping(method=RequestMethod.GET,value="/employee")
    List<Employee> getAllEmployee(){
        return employeeService.getAllEmployee();
    }

    @RequestMapping(method=RequestMethod.GET,value="/employee/{id}")
    Employee getEmployeeById(@PathVariable("id") int id){
        return employeeService.getEmployeeById(id);
    }

    @RequestMapping(method=RequestMethod.POST,value="/employee")
    void addEmployee(@RequestBody Employee employee)
    {
        employeeService.insert(employee);
    }

    @RequestMapping(method=RequestMethod.PUT,value="/employee/{id}")
    void updateEmployee(@PathVariable("id") int id ,@RequestBody Employee employee)
    {
        employeeService.update(id,employee);
    }

    @RequestMapping(value ="/employee/{id}" ,method = RequestMethod.DELETE)
    void deleteEmployee(@PathVariable("id") int id){
        employeeService.delete(id);
    }



}
