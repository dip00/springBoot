package com.itglance.springBoot.service;

import com.itglance.springBoot.entity.Employee;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class EmployeeService {
    List<Employee> employeeList = new ArrayList<>(Arrays.asList(
            new Employee(1,"ram","nepal",20000),
            new Employee(2,"ram","nepal",20000),
            new Employee(3,"ram","nepal",20000),
            new Employee(4,"ram","nepal",20000)
    ));
    public List<Employee> getAllEmployee(){
        return employeeList;
    }

    public Employee getEmployeeById(int id){
        for (Employee e:
             employeeList) {
            if(e.getId()==id)
                return e;
        }
        return null;
    }

    public void insert(Employee e){
        employeeList.add(e);
    }

    public void update (int id,Employee e){
        for(int i=0;i<employeeList.size();i++){
            if(employeeList.get(i).getId()==id)
                employeeList.set(i,e);
        }
    }

    public void delete (int id){
        for(int i=0;i<employeeList.size();i++){
            if(employeeList.get(i).getId()==id)
                employeeList.remove(i);
        }
    }
}
